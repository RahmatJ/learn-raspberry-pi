from flask import Flask, request
from motor import DualMotor

# define app name
app = Flask(__name__)

# instantiate Motor
motor= DualMotor()

# define routes
@app.route('/', methods=["GET"])
def home_page():
    return "This is home page"

@app.route('/start', methods=["POST"])
def start_engine():
    motor.initialize()
    return "Engine started"

@app.route('/move/<command>', methods=["POST"])
def move_motor(command):
    command = str(command).lower()
    if command == "forward":
        print("it's moving forward")
        motor.forward()
    elif command == "reverse":
        motor.backward()
        print("it's moving backward")
    else:
        return "invalid command", 400
    return f"move: {command}"

@app.route('/turn/<direction>', methods=["POST"])
def turn_motor(direction):
    direction = str(direction).lower()
    if direction == "left":
        motor.left()
        print("it's turning left")
    elif direction == "right":
        motor.right()
        print("it's turning right")
    else:
        return "invalid direction", 400
    return f"turn: {direction}"

@app.route('/speed/<int:stage>', methods=["POST"])
def change_speed(stage):
    stage = int(stage)
    if(stage == 0):
        motor.update_speed(stage)
        print('engine break, speed 0')
    elif(stage == 1):
        motor.update_speed(stage)
        print('engine speed 1')
    elif(stage == 2):
        motor.update_speed(stage)
        print('engine speed 2')
    elif(stage == 3):
        motor.update_speed(stage)
        print('engine speed 3')
    else: 
        return "invalid speed stage", 400
    return f"speed: {stage}"
    

# define main
if __name__=="__main__":
    host = "0.0.0.0"
    port = 5000
    app.run(host=host, port=port, debug=True)