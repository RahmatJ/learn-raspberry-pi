import RPi.GPIO as GPIO
from time import sleep
import random

in1 = 24
in2 = 23
en = 25

in3 = 17
in4 = 27
enb = 22

#set to GPIO number
GPIO.setmode(GPIO.BCM)
# init right pin
GPIO.setup(in1, GPIO.OUT)
GPIO.setup(in2, GPIO.OUT)
GPIO.setup(en, GPIO.OUT)
# init left pin
GPIO.setup(in3, GPIO.OUT)
GPIO.setup(in4, GPIO.OUT)
GPIO.setup(enb, GPIO.OUT)

# set low
GPIO.output(in1, GPIO.LOW)
GPIO.output(in2, GPIO.LOW)
GPIO.output(in3, GPIO.LOW)
GPIO.output(in4, GPIO.LOW)
# right
p=GPIO.PWM(en, 1000)
p.start(100)
# left
pb=GPIO.PWM(enb, 1000)
pb.start(100)

seq = (1,2,3)
dir_speed = {
    1: 50,
    2: 75,
    3: 100
}

try:
    while(1):
        rand_speed = random.choice(seq)
        speed = dir_speed[rand_speed]
        print("run forward, speed: %s" % speed)
        #right
        GPIO.output(in2, GPIO.HIGH)
        GPIO.output(in1, GPIO.LOW)
        #left
        GPIO.output(in4, GPIO.HIGH)
        GPIO.output(in3, GPIO.LOW)
        sleep(10)
        print('run stop a bit')
        #right
        GPIO.output(in1, GPIO.LOW)
        GPIO.output(in2, GPIO.LOW)
        #left
        GPIO.output(in3, GPIO.LOW)
        GPIO.output(in4, GPIO.LOW)
        sleep(10)
        rand_speed = random.choice(seq)
        speed = dir_speed[rand_speed]
        print("run backward, speed: %s" % speed)
        #right
        GPIO.output(in2, GPIO.LOW)
        GPIO.output(in1, GPIO.HIGH)
        #left
        GPIO.output(in4, GPIO.LOW)
        GPIO.output(in3, GPIO.HIGH)
        sleep(10)
except KeyboardInterrupt:
    pass

p.stop()
GPIO.cleanup()