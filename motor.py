from turtle import speed
import RPi.GPIO as GPIO
from threading import Timer

# 1. speed pwm mulai dari 0 -> max speed sekarang
# 2. if (forward) di klik, setiap ms, speed ditambah 1, sampai max (setiap ms nya bisa didiskusikan, jumlah tambahnya jg bisa didiskusikan)
# 3. if forward tidak di klik, setiap ms, speed dikurang 1, sampai 0 (mirip dengan diatas)

class DualMotor:

    speed_dir = {
        0: 0,
        1: 50,
        2: 75,
        3: 100
    }

    def __init__(self):
        # init pin number
        self._in1 = 24
        self._in2 = 23
        self._in3 = 17
        self._in4 = 27
        self._en_right = 25
        self._en_left = 22
        self._max_speed = 0

    # private
    def _pin_high(self,pin):
        GPIO.output(pin, GPIO.HIGH)

    def _pin_low(self,pin):
        GPIO.output(pin, GPIO.LOW)

    def _setup_pin(self,pin):
        GPIO.setup(pin, GPIO.OUT)

    def _set_max_speed(self, speed):
        self._max_speed = speed

    def _set_pwm_speed(self,pwm_instance, speed):
        pwm_instance.ChangeDutyCycle(speed)

    def _reset_pin(self):
        self._pin_low(self._in1)
        self._pin_low(self._in2)
        self._pin_low(self._in3)
        self._pin_low(self._in4)

    def _initiate_pin(self):
        # init right pin
        self._setup_pin(self._in1)
        self._setup_pin(self._in2)
        self._setup_pin(self._en_right)

        # init left pin
        self._setup_pin(self._in3)
        self._setup_pin(self._in4)
        self._setup_pin(self._en_left)

        # set low
        self._reset_pin()

    def _init_pwm(self, frequency, base_speed):
        # right
        self._pwm_right=GPIO.PWM(self._en_right, frequency)
        self._pwm_right.start(base_speed)
        # left
        self._pwm_left=GPIO.PWM(self._en_left, frequency)
        self._pwm_left.start(base_speed)

    def _stop(self):
        self._reset_pin()

    # public

    def initialize(self, frequency=1000, base_speed=0):
        GPIO.setmode(GPIO.BCM)
        self._initiate_pin()
        self._init_pwm(frequency, base_speed)
        
    def forward(self):
        self._reset_pin()
        self._pin_high(self._in2)
        self._pin_high(self._in4)

    def backward(self):
        self._reset_pin()
        self._pin_high(self._in1)
        self._pin_high(self._in3)

    def left(self):
        # left speed = 1/3 right speed
        self._set_pwm_speed(self._pwm_left, self._max_speed/3)
        s = Timer(1, self._set_pwm_speed(self._pwm_left, self._max_speed))
        s.start()

    def right(self):
        # right speed = 1/3 left speed
        self._set_pwm_speed(self._pwm_right, self._max_speed/3)
        s = Timer(1, self._set_pwm_speed(self._pwm_right, self._max_speed))
        s.start()

    def update_speed(self, num):
        if num < 0 or num >= 4:
            return
        
        spd = self.speed_dir[num]
        if num == 0:
            self._set_max_speed(spd)
            self._stop()
        else :
            self._set_max_speed(spd)
            
#TODO:
# 1. update change duty cycle when move forward or backward
# 2. check timer update spd left and right
# 3. add variable to make sure motor initialize, boolean
# 4. always check pwm if want to initiate it
# 5. add log for current speed, action. For now print is enough